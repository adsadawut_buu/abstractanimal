/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.animal;

/**
 *
 * @author อัษฎาวุฒิ
 */
public abstract class LandAnimal extends Animal {

    public LandAnimal(String name, int numOfLegs) {
        super(name, numOfLegs);
    }
    public abstract void run();
}
