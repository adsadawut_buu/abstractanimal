/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.animal;

/**
 *
 * @author อัษฎาวุฒิ
 */
public abstract class AquaticAnimal extends Animal {

    public AquaticAnimal(String name,int  numberOfLegs) {
        super(name,numberOfLegs );
    }

    public abstract void swim();
}
